
Laboratorium #8
===============

1. Korzystając z ostatniej pracy domowej przerobić formularz dodawania wpisu na formularz modelu Django
   z odpowiednią walidacją (wpis nie może być krótszy niż 10 znaków i dłuższy niż 200). Pole użytkownika
   powinno wypełniać się automatycznie aktualnie zalogowanym użytkownikiem.
   Aplikacja powinna współdziałać z *Apachem* na serwerze laboratoryjnym.

2. Dodać czas wpisu (nie powinnien się pojawiać na formularzu dodawania nowego wpisu)
   oraz możliwość tagowania wpisów (relacja wiele do wielu). Czas i tagi powinny być wyświetlane
   przy każdym wpisie.
   Uruchomić panel administracyjny z zarejestrowanymi modelami mikroblogu wraz z możliwością filtrowania
   i sortowania po czasie dodania oraz wyszukiwania po tagach.
